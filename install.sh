apt update
apt install -y \
    ca-certificates \
    curl \
    gnupg2 \
    lsb-release

mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

groupadd docker

usermod -aG docker $USER

newgrp docker

systemctl enable docker.service
systemctl enable containerd.service

systemctl start docker.service
systemctl start containerd.service

curl -fsSL https://www.percona.com/get/pmm | /bin/bash

wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb

dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb

apt update

apt install -y pmm2-client

pmm-admin config --server-insecure-tls --server-url=https://$1:$2@localhost
